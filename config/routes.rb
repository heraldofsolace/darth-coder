Rails.application.routes.draw do
  root 'static_pages#home'

  get "code", to: 'code#show'
  get "leaderboard", to: 'code#leaderboard'
  post "code/submit"
  post "code/skip"
  post "code/save_draft"
  post "code/get_draft"
  devise_for :teams
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
