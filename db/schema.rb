# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_05_034202) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "drafts", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.bigint "level_id", null: false
    t.string "language"
    t.text "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["level_id"], name: "index_drafts_on_level_id"
    t.index ["team_id"], name: "index_drafts_on_team_id"
  end

  create_table "level_details", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.bigint "level_id", null: false
    t.datetime "start_time", default: "2019-08-05 13:40:55"
    t.datetime "end_time", default: "2019-08-05 13:40:55"
    t.integer "wrong_attempts", default: 0
    t.boolean "skipped", default: false
    t.boolean "cleared", default: false
    t.decimal "score_awarded"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["level_id"], name: "index_level_details_on_level_id"
    t.index ["team_id", "level_id"], name: "index_level_details_on_team_id_and_level_id", unique: true
    t.index ["team_id"], name: "index_level_details_on_team_id"
  end

  create_table "levels", force: :cascade do |t|
    t.integer "level"
    t.text "question_body"
    t.string "question_answer"
    t.integer "max_score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["level"], name: "index_levels_on_level", unique: true
  end

  create_table "teams", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name_p1"
    t.string "name_p2"
    t.string "college_p1"
    t.string "college_p2"
    t.string "dept_p1"
    t.string "dept_p2"
    t.string "year_p1"
    t.string "year_p2"
    t.string "contact_p1"
    t.string "contact_p2"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_teams_on_email", unique: true
    t.index ["reset_password_token"], name: "index_teams_on_reset_password_token", unique: true
  end

  add_foreign_key "drafts", "levels"
  add_foreign_key "drafts", "teams"
  add_foreign_key "level_details", "levels"
  add_foreign_key "level_details", "teams"
end
