class CreateLevelDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :level_details do |t|
      t.references :team, null: false, foreign_key: true
      t.references :level, null: false, foreign_key: true
      t.datetime :start_time, default: DateTime.now
      t.datetime :end_time, default: DateTime.now
      t.integer :wrong_attempts, default: 0
      t.boolean :skipped, default: false
      t.boolean :cleared, default: false
      t.integer :score_awarded

      t.timestamps
    end

    add_index :level_details, [:team_id, :level_id], unique: true
  end
end
