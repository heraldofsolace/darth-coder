class ChangeScoreToRealInLevels < ActiveRecord::Migration[6.0]
  def change
    change_column :level_details, :score_awarded, :decimal
  end
end
