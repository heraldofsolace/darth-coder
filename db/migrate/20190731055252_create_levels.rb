class CreateLevels < ActiveRecord::Migration[6.0]
  def change
    create_table :levels do |t|
      t.integer :level, unique: true
      t.text :question_body
      t.string :question_answer
      t.integer :max_score

      t.timestamps


    end
    add_index :levels, :level, unique: true
  end
end
