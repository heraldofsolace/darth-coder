class CreateDrafts < ActiveRecord::Migration[6.0]
  def change
    create_table :drafts do |t|
      t.references :team, null: false, foreign_key: true
      t.references :level, null: false, foreign_key: true
      t.string :language
      t.text :code

      t.timestamps
    end
  end


end
