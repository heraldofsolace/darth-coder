class Team < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :level_details
  has_many :drafts

  def score
    LevelDetail.sum(:score).where(team: self)
  end

  def allowed_in_level(level)
    if level == 1
      true
    else
      @level_detail = LevelDetail.find_by(team: self, level: level - 1)
      @level_detail != nil && @level_detail.cleared

    end
  end
end
