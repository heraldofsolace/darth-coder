class CodeController < ApplicationController
  before_action :authenticate_team!

  def leaderboard
    @scores = LevelDetail.find_by_sql("
                SELECT team_id,
                name_p1,
                name_p2,
                total,
                DENSE_RANK() OVER (
                    ORDER BY total DESC
                ) AS rank FROM (
                    SELECT team_id,
                    SUM(score_awarded) AS total
                    FROM level_details
                    GROUP BY team_id
                ) AS s
                JOIN teams t
                ON t.id = s.team_id;

              ")
  end


  def submit
    level = params[:level]
    data = params[:data]
    @level = Level.find_by_level level
    result = {}
    result[:accepted] = @level.question_answer == data.chomp
    @level_detail = LevelDetail.find_by team: current_team, level: @level
    if result[:accepted]
      @level_detail.end_time = DateTime.now
      @level_detail.skipped = false
      @level_detail.cleared = true
      @level_detail.score_awarded = calculate_score @level, @level_detail
      @level_detail.save
      if level == 5
        result[:proceed_to] = nil
      else
        result[:proceed_to] = code_path(level: level.to_i + 1)
      end

    else
      @level_detail.wrong_attempts += 1
      @level_detail.save
    end

    render json: {result:  result}
  end
  def skip
    level = params[:level]
    @level = Level.find_by_level level
    @level_detail = LevelDetail.find_by team: current_team, level: @level
    @level_detail.end_time = DateTime.now
    @level_detail.skipped = true
    @level_detail.cleared = true
    @level_detail.score_awarded = calculate_score @level, @level_detail
    if @level_detail.save
      render json: { skipped: true }
    else
      render json: { skipped: false }
    end
  end

  def save_draft
    level = params[:level]
    language = params[:language]
    code = params[:code]
    @level = Level.find_by_level level

    draft = Draft.create team: current_team, level: @level, language: language, code: code
    if draft.save
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  def get_draft
    level = params[:level]
    language = params[:language]
    @level = Level.find_by_level level
    draft = Draft.order(created_at: :desc).find_by(team: current_team, level: @level, language: language)
    p draft
    if draft.nil?
      render json: { draft: nil }
    else
      render json: { draft: draft }
    end
  end

  def show
    if params[:level].nil?
      redirect_to code_path(level: 1) and return
    end
    if params[:level] == "6"
      render "done" and return
    end
    level = params[:level].to_i
    if current_team.allowed_in_level level
      @level = Level.find_by_level level
      if @level == nil
        redirect_to code_path(level: level - 1)
      end
      @level_detail = LevelDetail.where(team: current_team, level: @level).first_or_create
      @level_detail.start_time ||= DateTime.now
      @level_detail.save
    else
      redirect_to code_path(level: level - 1)
    end
  end

  private
  def calculate_score(level, level_detail)
    score = @level.max_score
    score -= @level_detail.wrong_attempts
    time_taken = @level_detail.end_time - @level_detail.start_time
    score *= (0.9) ** (time_taken / (1000 * 60))
    if level_detail.skipped
      score -= @level.max_score
    end
    score
  end
end
