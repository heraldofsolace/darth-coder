class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name_p1, :name_p2, :college_p1, :college_p2,
                                                          :dept_p1, :dept_p2, :year_p1, :year_p2, :contact_p1, :contact_p2])
  end
end
